<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
 
AppAsset::register($this);

$this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

  <head>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <?= Html::csrfMetaTags() ?>
    <title>Softy Pinko</title>
    <link rel="shortcut icon" href="images-main/shortcut_icon.png">
<!--
SOFTY PINKO
https://templatemo.com/tm-535-softy-pinko
-->

    <?php $this->head() ?>
    </head>
    
    <body>
    <?php $this->beginBody() ?>
    <!-- ***** Preloader Start ***** -->
    <!-- <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>   -->
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="#" class="logo">
                            <img src="images-main/logo.png" alt="Softy Pinko"/>
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="/" class="active">Главная</a></li>
                            <li><a href="#features">О нас</a></li>
                            <li><a href="#work-process">Услуги</a></li>
                            <li><a href="#testimonials">Комментарии</a></li>
                            <li><a href="#pricing-plans">Ценовые планы</a></li>
                            <li><a href="#blog">Новости</a></li>
<!--                            <li><a href="#contact-us">Contact Us</a></li>-->
                            <li><a href="map">Обратная связь</a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <?= $content ?>
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright"></p>
                </div>
            </div>
        </div>
    </footer>
    
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>