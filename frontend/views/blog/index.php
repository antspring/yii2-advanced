<?php

use common\models\BlogEntries;
use common\models\BlogEntriesCard;
use common\models\HeaderText;
use common\models\HomeFeature;
use common\models\About;
use common\models\PricingPlans;
use common\models\PricingPlansCard;
use common\models\Statistics;
use common\models\Testimonials;
use common\models\TestimonialsCard;
use common\models\WorkProcess;
use common\models\WorkProcessCard;

/** @var $header_text HeaderText */

/** @var $home_feature HomeFeature */

/** @var $about1 About */
/** @var $about2 About */

/** @var $work_process WorkProcess */
/** @var $work_process_card WorkProcessCard */

/** @var $testimonials Testimonials */
/** @var $testimonials_card TestimonialsCard */

/** @var $pricingPlans PricingPlans */
/** @var $pricingPlansCard PricingPlansCard*/
/** @var $statistics Statistics */
/** @var $blogEntries BlogEntries */
/** @var $blogEntriesCard BlogEntriesCard */

?>
<div class="welcome-area" id="welcome">
    <div class="header-text">
        <div class="container">
            <div class="row">
                <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                    <h1><?= $header_text->text ?></h1>
                    <p><?= $header_text->description ?></p>
                    <a href="#features" class="main-button-slider"><?= $header_text->button_text ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section home-feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    <?php
                    $i = 0;
                    foreach ($home_feature as $item):
                        $i++;
                    if($i == 4)
                        break;
                    ?>
                    <!-- ***** Features Small Item Start ***** -->
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12"
                         data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                        <div class="features-small-item">
                            <div class="icon">
                                <i><img src="/images/<?= $item->image ?>" alt=""></i>
                            </div>
                           <h5 class="features-title"><?= $item->title ?></h5>
                            <p><?= $item->description ?></p>
                        </div>
                    </div>
                    <?php endforeach?>
                    <!-- ***** Features Small Item End ***** -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Small End ***** -->

<!-- ***** Features Big Item Start ***** -->
<section class="section padding-top-70 padding-bottom-0" id="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 align-self-center"
                 data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                <img src="/images/<?=$about1->image ?>" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-top-fix">
                <div class="left-heading">
                    <h2 class="section-title"><?=$about1->title ?></h2>
                </div>
                <div class="left-text">
                    <p><?=$about1->description ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Big Item End ***** -->

<!-- ***** Features Big Item Start ***** -->
<section class="section padding-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-bottom-fix">
                <div class="left-heading">
                    <h2 class="section-title"><?=$about2->title ?></h2>
                </div>
                <div class="left-text">
                    <p><?=$about2->description ?></p>
                </div>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5 col-md-12 col-sm-12 align-self-center mobile-bottom-fix-big"
                 data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                <img src="/images/<?=$about2->image ?>" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
        </div>
    </div>
</section>
<!-- ***** Features Big Item End ***** -->

<!-- ***** Home Parallax Start ***** -->
<section class="mini" id="work-process">
    <div class="mini-content">
        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <div class="info">
                        <h1><?= $work_process->text ?></h1>
                        <p><?= $work_process->description ?></p>
                    </div>
                </div>
            </div>

            <!-- ***** Mini Box Start ***** -->
            <div class="row">
                <?php
                $i = 0;
                foreach ($work_process_card as $item):
                    $i++;
                if($i == 7)
                    break;
                    ?>
                <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                    <a href="#" class="mini-box">
                        <i><img src="/images/<?=$item->image ?>" alt=""></i>
                        <strong><?=$item->title ?></strong>
                        <span><?=$item->description ?></span>
                    </a>
                </div>
                <?php endforeach;?>
            </div>
            <!-- ***** Mini Box End ***** -->
        </div>
    </div>
</section>
<!-- ***** Home Parallax End ***** -->

<!-- ***** Testimonials Start ***** -->
<section class="section" id="testimonials">
    <div class="container">
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title"><?=$testimonials->title ?></h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p><?=$testimonials->description?></p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <!-- ***** Testimonials Item Start ***** -->
            <?php
            $i = 0;
            foreach ($testimonials_card as $item):
                $i++;
            if($i > 3){
                break;
            }
                ?>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="team-item">
                    <div class="team-content">
                        <i><img src="images-main/testimonial-icon.png" alt=""></i>
                        <p><?=$item->text?></p>
                        <div class="user-image">
                            <img src="images/<?=$item->image?>" alt="">
                        </div>
                        <div class="team-info">
                            <h3 class="user-name"><?=$item->name ?></h3>
                            <span><?=$item->position ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- ***** Testimonials End ***** -->

<!-- ***** Pricing Plans Start ***** -->
<section class="section colored" id="pricing-plans">
    <div class="container">
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title"><?=$pricingPlans->title ?></h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p><?=$pricingPlans->description ?></p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <!-- ***** Pricing Item Start ***** -->
            <?php
            $i = 0;
            foreach ($pricingPlansCard as $item):
                $i++;
                if($i > 3){
                    break;
                }
            ?>
            <div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                <div class="pricing-item">
                    <div class="pricing-header">
                        <h3 class="pricing-title"><?=$item->title ?></h3>
                    </div>
                    <div class="pricing-body">
                        <div class="price-wrapper">
                            <span class="currency">$</span>
                            <span class="price"><?=$item->price ?></span>
                            <span class="period">В месяц</span>
                        </div>
                        <ul class="list">
                            <?=$item->specifications ?>
                        </ul>
                    </div>
                    <div class="pricing-footer">
                        <a href="#" class="main-button"><?=$item->button_text ?></a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- ***** Pricing Plans End ***** -->

<!-- ***** Counter Parallax Start ***** -->
<section class="counter">
    <div class="content">
        <div class="container">
            <div class="row">
                <?php
                $i = 0;
                foreach ($statistics as $item):
                    $i++;
                if($i > 4)
                    break;
                ?>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="count-item decoration-bottom">
                        <strong><?=$item->number ?></strong>
                        <span><?=$item->text ?></span>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- ***** Counter Parallax End ***** -->

<!-- ***** Blog Start ***** -->
<section class="section" id="blog">
    <div class="container">
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title"><?=$blogEntries->title ?></h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p><?=$blogEntries->description ?></p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <?php
            foreach ($blogEntriesCard as $item):
            ?>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="blog-post-thumb">
                    <div class="img">
                        <img src="images/<?= $item->image?>" alt="">
                    </div>
                    <div class="blog-content">
                        <h3>
                            <a href="#"><?=$item->title ?></a>
                        </h3>
                        <div class="text">
                            <?=$item->description ?>
                        </div>
                        <a href="#" class="main-button"><?=$item->button_text ?></a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- ***** Blog End ***** -->