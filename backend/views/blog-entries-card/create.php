<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogEntriesCard */

$this->title = 'Create Blog Entries Card';
$this->params['breadcrumbs'][] = ['label' => 'Blog Entries Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-entries-card-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
