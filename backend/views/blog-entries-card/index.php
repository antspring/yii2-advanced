<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogEntriesCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blog Entries Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-entries-card-index">


    <p>
        <?= Html::a('Create Blog Entries Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'smallImage:image',
            'title',
            'description',
            'button_text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
