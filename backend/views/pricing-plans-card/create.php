<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricingPlansCard */

$this->title = 'Create Pricing Plans Card';
$this->params['breadcrumbs'][] = ['label' => 'Pricing Plans Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-plans-card-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
