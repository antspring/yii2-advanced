<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HeaderText */

$this->title = 'Update Header Text: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Header Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="header-text-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
