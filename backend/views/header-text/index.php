<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HeaderTextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Header Texts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="header-text-index">


    <p>
        <?= Html::a('Создать Header text', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'text',
            'description',
            'button_text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
