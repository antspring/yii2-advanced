<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HeaderText */

$this->title = 'Create Header Text';
$this->params['breadcrumbs'][] = ['label' => 'Header Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="header-text-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
