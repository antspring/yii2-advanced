<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomeFeature */

$this->title = 'Update Home Feature: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-feature-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
