<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pricing_plans_card".
 *
 * @property int $id
 * @property int|null $status
 * @property string|null $title
 * @property int|null $price
 * @property string|null $specifications
 * @property string|null $button_text
 */
class PricingPlansCard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pricing_plans_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'price'], 'double'],
            [['title'], 'string', 'max' => 15],
            [['specifications'], 'string', 'max' => 300],
            [['button_text'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'title' => 'Заголовок',
            'price' => 'Цена',
            'specifications' => 'Характеристики',
            'button_text' => 'Текст кнопки',
        ];
    }
}
