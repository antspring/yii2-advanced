<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "map".
 *
 * @property int|null $status
 * @property int $id
 * @property string|null $text
 */
class Map extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['text'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'text' => 'Код карты',
        ];
    }
}
