<?php

use yii\db\Migration;

/**
 * Class m210909_092634_create_pricing_plans_card
 */
class m210909_092634_create_pricing_plans_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pricing_plans_card',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'title' => $this->string(15),
            'price' => $this->integer(),
            'specifications' => $this->string(150),
            'button_text' => $this->string(20)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pricing_plans_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092634_create_pricing_plans_card cannot be reverted.\n";

        return false;
    }
    */
}
