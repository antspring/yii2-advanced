<?php

use yii\db\Migration;

/**
 * Class m210714_145805_create_map
 */
class m210714_145805_create_map extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('map', [
            'status' => $this->boolean(),
            'id' => $this->primaryKey(),
            'text' => $this->string(1000)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('map');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210714_145805_create_map cannot be reverted.\n";

        return false;
    }
    */
}
