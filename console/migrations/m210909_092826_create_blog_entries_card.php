<?php

use yii\db\Migration;

/**
 * Class m210909_092826_create_blog_entries_card
 */
class m210909_092826_create_blog_entries_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blog_entries_card',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'image' => $this->string(100),
            'title' => $this->string(10),
            'description' => $this->string( 30),
            'button_text' => $this->string(20)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('blog_entries_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092826_create_blog_entries_card cannot be reverted.\n";

        return false;
    }
    */
}
