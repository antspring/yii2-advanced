<?php

use yii\db\Migration;

/**
 * Class m210909_092753_create_statistics
 */
class m210909_092753_create_statistics extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('statistics',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'number' => $this->integer(),
            'text' => $this->string(20)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('statistics');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092753_create_statistics cannot be reverted.\n";

        return false;
    }
    */
}
